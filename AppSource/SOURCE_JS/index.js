
import _ from 'lodash';
import printMe from './print.js';

import '../SOURCE_STYLES/straight.css';
import jsonData from './test.json';
import xmlData from './test.xml';
import BaseImg from '../SOURCE_IMAGES/example.png';

if (process.env.NODE_ENV && process.env.NODE_ENV !== 'production') {
	console.log('index.js is Running under DEV Environment');
}


function component() {
	let element = document.createElement('div');

	element.innerHTML = _.join(['Hello', 'webpack', jsonData.test_item, xmlData.note.body], ' ');
	element.classList.add('hello');

	let btn = document.createElement('button');
	btn.innerHTML = 'Click me and check the console!';
	btn.onclick = printMe;
	element.appendChild(btn);

	const sampleImage = new Image();
	sampleImage.src = BaseImg;
	sampleImage.classList.add('in-message-image');
	element.appendChild(sampleImage);

	return element;
}

let messageSpace = document.getElementById("message_space");
messageSpace.appendChild(component());
