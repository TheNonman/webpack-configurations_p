module.exports = function (grunt, options)
{
	return {
		options: {
			sourceMap: false,
			presets: ['env', 'react'],
			"plugins": ["transform-es2015-modules-umd"]
		},
		dist: {
			files: [{
				expand: true,
				cwd: '<%= BuildConfigs.src.js %>',
				src: ['react_build-time.js', 'preact_build-time.js'],
				dest: '<%= BuildConfigs.temp.js %>'
			},
			{
				expand: true,
				cwd: '<%= BuildConfigs.src.js %>',
				src: ['MasteringReactProjects/**/*.js'],
				dest: '<%= BuildConfigs.temp.js %>'
			}
		]
		}
	}
};
