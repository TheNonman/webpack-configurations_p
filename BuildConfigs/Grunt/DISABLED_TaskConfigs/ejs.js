module.exports = function (grunt, options)
{
	return {
		all: {
			src: [	' **/*.ejs',
					'!_partials/**/*',
					'!_layouts/**/*',
					'!_blocks/**/*'
			],
			cwd: '<%= BuildConfigs.src.html_ejs %>/',
			dest: '<%= BuildConfigs.staging.root %>/',
			expand: true,
			ext: '.html',
			options: {
				title: 'EJS-locals',
				headline: 'EmbeddedJS with locals',
				paragraph: 'A Grunt task for compiling EJS templates with the taste of layouts, blocks and partials.'
			}
		}
	}
};
