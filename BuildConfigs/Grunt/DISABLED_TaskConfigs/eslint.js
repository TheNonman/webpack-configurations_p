module.exports = {
	options: {
		configFile: './BuildConfigs/eslint.json',
		reset: true
	},
	grunt_file: {
		options: {
			envs: ["node", "commonjs"]
		},
		files: {
			src:['gruntfile.js']
		}
	},
	grunt_custom_tasks: {
		options: {
			envs: ["node", "commonjs", "es6"]
		},
		files: {
			src:['BuildConfigs/Grunt/CustomTasks/**/*.js']
		}
	},
	main_js: [
				'<%= BuildConfigs.src.js %>/**/*.js',
				'!<%= BuildConfigs.src.js_3P %>/**/*.js',
				'!<%= BuildConfigs.src.js %>/**/react_build-time.js',
				'!<%= BuildConfigs.src.js %>/**/preact_build-time.js',
				'!<%= BuildConfigs.src.js %>/**/react_rollup_package.js',
				'!<%= BuildConfigs.src.js %>/**/preact_rollup_package.js',
				'!<%= BuildConfigs.src.js %>/**/framerX.js',
				'!<%= BuildConfigs.src.js %>/**/_testRollup_CJS_ReactComponents.js',
				'!**/MasteringReactProjects/**',
				'!**/ReactDocs/**',
				'!**/ReactComponents_CJS/**'
	]
};
