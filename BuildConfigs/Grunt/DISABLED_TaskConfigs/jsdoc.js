module.exports = function (grunt, options)
{
	return {
		dist: {
			src: [	'<%= BuildConfigs.src.js %>/**/*.js',
					'README.md',
					'!**/a_third_party/**'
			],
			options: {
				destination: 'Documentation/API_Docs'
			}
		}
	}
};
