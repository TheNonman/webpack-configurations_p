module.exports = function (grunt, options)
{
	return {
		sourceDebug: {
			src: ['gruntfile.js',
				'<%= BuildConfigs.src.js %>/**/*.js',
				'!<%= BuildConfigs.src.js_3P %>/**/*.js',
				'!<%= BuildConfigs.src.js %>/**/react_build-time.js',
				'!<%= BuildConfigs.src.js %>/**/preact_build-time.js',
				'!<%= BuildConfigs.src.js %>/**/react_rollup_package.js',
				'!<%= BuildConfigs.src.js %>/**/preact_rollup_package.js',
				'!<%= BuildConfigs.src.js %>/**/_testRollup_CJS_ReactComponents.js',
				'!**/MasteringReactProjects/**',
				'!**/ReactComponents_CJS/**'
			],
			options: options.JSHintOptions.Debug
		},
		sourceProduction: {
			src: ['gruntfile.js',
				'<%= BuildConfigs.src.js %>/**/*.js',
				'!<%= BuildConfigs.src.js %>/**/react_build-time.js',
				'!<%= BuildConfigs.src.js %>/**/preact_build-time.js',
				'!<%= BuildConfigs.src.js %>/**/react_packaged.js',
				'!<%= BuildConfigs.src.js %>/**/preact_packaged.js',
				'!<%= BuildConfigs.src.js %>/**/_testRollup_CJS_ReactComponents.js',
				'!**/MasteringReactProjects/**',
				'!**/ReactComponents_CJS/**'
			],
			options: options.JSHintOptions.Production
		},
		grunt: {
			src: ['gruntfile.js'],
			options: options.JSHintOptions.Production
		}
	}
};
