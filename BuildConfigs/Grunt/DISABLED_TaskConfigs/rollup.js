

var rollup_Babel = require('rollup-plugin-babel');
var rollup_CommonJS = require('rollup-plugin-commonjs');
var rollup_NodeResolve = require('rollup-plugin-node-resolve');
var rollup_Replace = require('rollup-plugin-replace');

module.exports = function (grunt, options)
{
	return {
		options: {
			babelrc: false
		},
		libraries: {
			files: {
				'_CommonJSModules/bundle.js': 'AppSource/SOURCE_JS/ReactComponents_CJS/react_packaged.js'
			},
			options: {
				format: 'cjs',
				external: ['react', 'react-proptypes'],
				plugins: function(){
					return [
						rollup_NodeResolve(),
						rollup_Babel({
							babelrc: false,
							presets: [['env', { modules: false }], "react"],
							plugins: ["transform-react-jsx"],
							exclude: 'node_modules/**'
						})
					];
				}
			}
		},
		product_react: {
			files: {
				'<%= BuildConfigs.staging.js %>/react_rollup_bundle.js': '<%= BuildConfigs.src.js %>/react_rollup_package.js'
			},
			options: {
				format: 'umd',
				banner: '/* Start Bundle - my-library version 0.0.1 */',
				footer: '/* End Bundle */',
				sourceMap: 'inline',
				plugins: function(){
					return [
						rollup_NodeResolve(),
						rollup_Babel({// JSX Must be transformed before the CommonJS Plugin is executed
							babelrc: false,
							presets: [['env', { modules: false }], "react"],
							plugins: ["transform-react-jsx"],
							exclude: 'node_modules/**'
						}),
						rollup_CommonJS(),
						rollup_Replace({
							'process.env.NODE_ENV': JSON.stringify( 'production' )
						}),
					];
				}
			}
		},
		product_preact: {
			files: {
				'<%= BuildConfigs.staging.js %>/preact_rollup_bundle.js': '<%= BuildConfigs.src.js %>/preact_rollup_package.js'
			},
			options: {
				format: 'umd',
				banner: '/* Start Bundle - my-library version 0.0.1 */',
				footer: '/* End Bundle */',
				sourceMap: 'inline',
				plugins: function(){
					return [
						rollup_NodeResolve(),
						rollup_Babel({// JSX Must be transformed before the CommonJS Plugin is executed
							babelrc: false,
							presets: [['env', { modules: false }], "react"],
							plugins: ["transform-react-jsx"],
							exclude: 'node_modules/**'
						}),
						rollup_CommonJS(),
						rollup_Replace({
							'process.env.NODE_ENV': JSON.stringify( 'production' )
						}),
					];
				}
			}
		},
		reactdocs: {
			files: {
				'<%= BuildConfigs.staging.js %>/ReactDocs/MultihelloWidget.js'
					: '<%= BuildConfigs.src.js %>/ReactDocs/MultihelloWidget.js'
			},
			options: {
				format: 'umd',
				banner: '/* Start Bundle - my-library version 0.0.1 */',
				footer: '/* End Bundle */',
				sourceMap: 'inline',
				plugins: function(){
					return [
						rollup_NodeResolve(),
						rollup_Babel({// JSX Must be transformed before the CommonJS Plugin is executed
							babelrc: false,
							presets: [['env', { modules: false }], "react"],
							plugins: ["transform-react-jsx"],
							exclude: 'node_modules/**'
						}),
						rollup_CommonJS(),
						rollup_Replace({
							'process.env.NODE_ENV': JSON.stringify( 'production' )
						}),
					];
				}
			}
		},
	}
};
