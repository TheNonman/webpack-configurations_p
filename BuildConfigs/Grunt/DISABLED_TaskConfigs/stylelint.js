
module.exports = function (grunt, options)
{
	return {
		css: {
			options: {
				configFile: 'BuildConfigs/stylelint.css.js',
				formatter: 'string',
				ignoreDisables: false,
				failOnError: true,
				outputFile: '',
				reportNeedlessDisables: false
			},
			src: [
				'<%= BuildConfigs.src.css %>/**/*.css',
				'!<%= BuildConfigs.src.css %>/StylelintFailTests/**',
				'!<%= BuildConfigs.src.css %>/a_third_party/**'
			]
		},
		temp_css: {
			options: {
				configFile: 'BuildConfigs/stylelint.css.js',
				formatter: 'string',
				ignoreDisables: false,
				failOnError: true,
				outputFile: '',
				reportNeedlessDisables: false
			},
			src: [
				'<%= BuildConfigs.temp.css %>/**/*.css',
				'!<%= BuildConfigs.temp.css %>/StylelintFailTests/**',
				'!<%= BuildConfigs.temp.css %>/a_third_party/**'
			]
		},
		scss: {
			options: {
				configFile: 'stylelint.config.scss.js',
				formatter: 'string',
				ignoreDisables: false,
				failOnError: true,
				outputFile: '',
				reportNeedlessDisables: false,
				syntax: 'scss'
			},
			src: [
				'<%= BuildConfigs.src.css %>/**/*.scss',
				'!<%= BuildConfigs.src.css %>/StylelintFailTests/**',
				'!<%= BuildConfigs.src.css %>/a_third_party/**'
			]
		},
		css_fail: {
			options: {
				configFile: 'BuildConfigs/stylelint.css.js',
				formatter: 'string',
				ignoreDisables: false,
				failOnError: true,
				outputFile: '',
				reportNeedlessDisables: false
			},
			src: [
				'<%= BuildConfigs.src.css %>/**/*.css',
				'!<%= BuildConfigs.src.css %>/a_third_party/**'
			]
		},
		scss_fail: {
			options: {
				configFile: 'stylelint.config.scss.js',
				formatter: 'string',
				ignoreDisables: false,
				failOnError: true,
				outputFile: '',
				reportNeedlessDisables: false,
				syntax: 'scss'
			},
			src: [
				'<%= BuildConfigs.src.css %>/**/*.scss',
				'!<%= BuildConfigs.src.css %>/a_third_party/**'
			]
		},
		third_check: {
			// Hummingbird Blacklist ONLY analysis of third party CSS/SCSS
			options: {
				configFile: 'BuildConfigs/stylelint.hummingbird_lib_analysis.js',
				formatter: 'string',
				ignoreDisables: false,
				failOnError: true,
				outputFile: '',
				reportNeedlessDisables: false
			},
			src: [
				'<%= BuildConfigs.src.css_3P %>/**/*.scss',
			]
		},
	}
}
