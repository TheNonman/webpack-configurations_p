module.exports = function (grunt, options)
{
	return {
		options: {
			proxy: 'localhost:3300',
			port: 3000,
			watchTask: true,
			browser: ['chrome', 'firefox', 'safari'],
			open: 'local',
			ghostMode: {
				clicks: true,
				forms: true,
				scroll: true
			},
			ui: {
				port: 3001
			}
		},
		dev_mac: {
			bsFiles: {
				src: '<%= BuildConfigs.staging.root %>/**/*'
			},
			options: {
				browser: ['google chrome']
			}
		},
		dev_mac_all: {
			bsFiles: {
				src: '<%= BuildConfigs.staging.root %>/**/*'
			},
			options: {
				browser: ['google chrome', 'firefox', 'safari']
			}
		},
		dev_nix: {
			bsFiles: {
				src: '<%= BuildConfigs.staging.root %>/**/*'
			},
			options: {
				browser: ['chrome']
			}
		},
		dev_nix_all: {
			bsFiles: {
				src: '<%= BuildConfigs.staging.root %>/**/*'
			},
			options: {
				browser: ['chrome', 'firefox']
			}
		},
		dev_win: {
			bsFiles: {
				src: '<%= BuildConfigs.staging.root %>/**/*'
			},
			options: {
				browser: ['chrome']
			}
		},
		dev_win_all: {
			bsFiles: {
				src: '<%= BuildConfigs.staging.root %>/**/*'
			},
			options: {
				browser: ['chrome', 'firefox']
			}
		}
	}
};
