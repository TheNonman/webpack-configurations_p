module.exports = function (grunt, options)
{
	return {
		full: {
			options: {
				'no-write': false
			},
			src: [	'<%= BuildConfigs.temp.root %>',
					'<%= BuildConfigs.staging.root %>'
			]
		},
		full_test: {
			options: {
				'no-write': true
			},
			src: [	'<%= BuildConfigs.temp.root %>',
					'<%= BuildConfigs.staging.root %>'
			]
		},
		strip_to_source: {
			options: {
				'no-write': false
			},
			src: [	'<%= BuildConfigs.temp.root %>',
					'<%= BuildConfigs.staging.root %>',
					'.sass-cache',
					'__BuildLogs',
					'AppSource/bower_components',
					'node_modules'
			]
		}
	}
};
