module.exports = function (grunt, options)
{
	return {
		bower_components_debug: {
			files: []
		},
		image_files_ToTemp: {
			files: [{
				expand: true,
				cwd: '<%= BuildConfigs.src.images %>',
				src: ['**/*.jpg', '**/*.png', '**/*.gif', '**/*.svg'],
				dest: '<%= BuildConfigs.temp.images %>'
			}]
		},
		image_files_ToStaging: {
			files: [{
				expand: true,
				cwd: '<%= BuildConfigs.temp.images %>',
				src: ['**/*.jpg', '**/*.png', '**/*.gif', '**/*.svg'],
				dest: '<%= BuildConfigs.staging.images %>'
			}]
		},
		pre_css_source: {
			files: [{
				expand: true,
				cwd: '<%= BuildConfigs.src.css %>',
				src: ['**/*.scss', '**/*.sass'],
				dest: '<%= BuildConfigs.temp.css_pre_source %>'
			}]
		}
	}
};
