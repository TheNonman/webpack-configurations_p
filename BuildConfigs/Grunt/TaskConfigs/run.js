module.exports = function (grunt, options)
{
	return {
		launch_hb: {
			cmd: 'node',
			args: [
				'BuildConfigs/HummingbirdScripts/launch_hb.js',
				'_Staging/index.html'
			]
		}
	}
};
