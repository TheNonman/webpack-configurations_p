module.exports = function (grunt, options)
{
	return {
		css_ToStaging: {
			files: [{
					cwd: '<%= BuildConfigs.temp.css %>',
					src: ['**/*.css'],
					dest: '<%= BuildConfigs.staging.css %>'
				},
				{
					cwd: '<%= BuildConfigs.temp.css_3P %>',
					src: ['**/*', '!**/*.css'],
					dest: '<%= BuildConfigs.staging.css_3P %>'
				}
			],
			pretend: false,
			verbose: true,
			updateAndDelete: true
		},
		css_ToTemp: {
			files: [{
					cwd: '<%= BuildConfigs.src.css %>',
					src: ['**/*.css'],
					dest: '<%= BuildConfigs.temp.css %>'
				},
				{
					cwd: '<%= BuildConfigs.src.css_3P %>',
					src: ['**/*', '!**/*.css', '!**/*.scss'],
					dest: '<%= BuildConfigs.temp.css_3P %>'
				}
			],
			pretend: false,
			verbose: true,
			updateAndDelete: false
		},
		fonts_ToStaging: {
			files: [{
				cwd: '<%= BuildConfigs.src.fonts %>',
				src: ['**/*.eot', '**/*.otf', '**/*.ttf', '**/*.woff', '**/*.woff2'],
				dest: '<%= BuildConfigs.staging.fonts %>'
			}],
			pretend: false,
			verbose: true,
			updateAndDelete: false
		},
		fonts_built_ToStaging: {
			files: [{
				cwd: '<%= BuildConfigs.temp.fonts_built %>',
				src: ['**/*.eot', '**/*.otf', '**/*.ttf', '**/*.woff', '**/*.woff2'],
				dest: '<%= BuildConfigs.staging.fonts %>'
			}],
			pretend: false,
			verbose: true,
			updateAndDelete: false
		},
		html_ToStaging: {
			files: [{
				cwd: '<%= BuildConfigs.src.html %>',
				src: ['**/*.html', '!**/*_template.html'],
				dest: '<%= BuildConfigs.staging.root %>'
			}],
			pretend: false,
			verbose: true,
			updateAndDelete: false
		},
		js_ToStaging: {
			files: [{
					cwd: '<%= BuildConfigs.temp.js %>',
					src: ['**/*.js', '!**/ReactComponents/**'],
					dest: '<%= BuildConfigs.staging.js %>'
				},
				{
					cwd: '<%= BuildConfigs.temp.js %>',
					src: ['**/*', '!**/*.js'],
					dest: '<%= BuildConfigs.staging.js %>'
				}
			],
			pretend: false,
			verbose: true,
			updateAndDelete: true
		},
		js_ToTemp: {
			files: [{
				cwd: '<%= BuildConfigs.src.js %>',
				src: ['**/*',
					'!**/__NOT_IN_USE/**',
					'!**/amd_modules/**',
					'!**/ReactComponents/**',
					'!**/MasteringReactProjects/**',
					'!**/ReactDocs/**',
					'!**/*.coffee',
					'!**/react_rollup_package.js',
					'!**/preact_rollup_package.js'
				],
				dest: '<%= BuildConfigs.temp.js %>'
			}],
			pretend: false,
			verbose: true,
			updateAndDelete: false
		},
		js3P_ToTemp: {
			files: [{
				cwd: '<%= BuildConfigs.src.js_3P %>',
				src: ['**/*',
					'!**/__NOT_IN_USE/**',
					'!**/amd_modules/**',
					'!**/*.coffee'
				],
				dest: '<%= BuildConfigs.temp.js_3P %>'
			}],
			pretend: false,
			verbose: true,
			updateAndDelete: false
		},
		openFl_ToStaging: {
			files: [{
				cwd: '<%= BuildConfigs.temp.root %>/openfl/html5/bin',
				src: ['**/*'],
				dest: '<%= BuildConfigs.staging.root %>/openfl_package'
			}],
			pretend: false,
			verbose: true,
			updateAndDelete: false
		}
	}
};
