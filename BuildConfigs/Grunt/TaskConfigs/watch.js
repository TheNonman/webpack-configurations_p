module.exports = function (grunt, options)
{
	return {
		options: {},
		sass: {
			files: ['<%= BuildConfigs.src.css %>/**/*.scss'],
			tasks: ['sass']
		},
		html: {
			files: ['<%= BuildConfigs.src.html %>/**/*.html', '!**/*_template.html'],
			tasks: ['sync:html_ToStaging']
		},
		html_ejs_templates: {
			files: ['<%= BuildConfigs.src.html_ejs %>/**/*.ejs'],
			tasks: ['ejs']
		}
	}
};
