
const merge = require('webpack-merge');

// Load the webpack.config.js file to base the configuration on
const webpackConfig = require('../../WebPack/webpack.common.config.js');


module.exports = function (grunt, options)
{
	return {
		options: {
			stats: !process.env.NODE_ENV || process.env.NODE_ENV === 'development',
			mode: 'development'
		},
		dev: merge(webpackConfig, {
			// Unchanged Defaults
		}),
		dev_watch: merge(webpackConfig, {
			watch: true
		}),
		prod: merge(webpackConfig, {
			mode: 'production',
			output: {
				publicPath: '/'
			},
			devtool: 'source-map'
		})
	}
};
