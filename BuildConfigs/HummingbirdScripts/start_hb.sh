#!/usr/bin/env bash

printf "BASH CONTEXT : start.mac.sh\n";
printf "Launch Hummingbird Player Using BASH";
printf "\n\nBash PWD :  $PWD";

printf "\n\nArg 0 (running script) :  $0";
printf "\nArg 1 - Hummingbird Player Path :  $1";
printf "\nArg 2 - Project Root :  $2";
printf "\nArg 3 - Hummingbird Starting Page :  $3";
printf "\nArg 4 - Hummingbird Extra Args :  $4";


printf "\n\nLaunching now...\n\n";

"$1" --args --root="$2" --url=coui://"$3" $4
