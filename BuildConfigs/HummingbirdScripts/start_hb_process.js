

const fs = require('fs');
const { spawn, exec } = require('child_process');
const os = require('os');


const outputDebug_BOOL = true;
const npmCmd = os.platform().startsWith('win') ? 'npm.cmd' : 'npm';
const runtimePlatform = os.platform().startsWith('win') ? 'win' : 'nix';
let HummingbirdProcess = null;


function DebugLog( a_string )
{
	if (!outputDebug_BOOL)			return;
	console.log(a_string);
};


DebugLog("*** NODE JS CONTEXT: start.js ***\n");

DebugLog("Launching Hummingbird Player");
DebugLog("==========================================================================================");
DebugLog("==========================================================================================\n\n");

DebugLog("Project Configured Environment VARs");
DebugLog("--------------------------------------------------------------------------------------------");
DebugLog("HUMMINGBIRD_PLAYER_PATH :  " + process.env.HUMMINGBIRD_PLAYER_PATH);

let projectRoot = process.cwd();
let projectScripts = null;

DebugLog("\nProject CWD :  " + projectRoot);


// Try Moving into and Setting the scripts Directory
try {
	process.chdir('BuildScripts');
	projectScripts = process.cwd();
	DebugLog('Project Scripts Dir :  ' + projectScripts);
	// Return to Active CWD
	process.chdir('../');
}
catch (err) {
	DebugLog('WARNING: No Project/Sub-Module "scripts" directory could not be found in :');
	DebugLog(projectRoot);
	DebugLog('chdir: ' + err);
}


// Verify that the Configured Hummingbird Player path is Valid on the System
if(!fs.existsSync(process.env.HUMMINGBIRD_PLAYER_PATH)) {
	console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

	console.log("\nERROR");
	console.log("\nThe configured path to the Hummingbird Player is not valid on your system !");
	console.log("Path: " + process.env.HUMMINGBIRD_PLAYER_PATH);
	console.log("\nMake sure you have a '_local.env' file (hint: copy and rename the default.env). Also, make sure it contain a valid path to an executable, standalone, Hummingbird Player on your local system.");

	console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
	return;
}


DebugLog("\nReceived Command Line Args :  ");
process.argv.forEach(function (val, index, array) {
	DebugLog(index + ': ' + val);
});

// Check for Shell Script Usage
if (process.env.USE_SHELL_SCRIPT && process.env.USE_SHELL_SCRIPT.length > 0) {
	if (!process.env.USE_SHELL || process.env.USE_SHELL.length <= 0) {
		console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

		console.log("\nWARNING");
		console.log("\nThe configured ENV contains a shell script to use, but USE_SHELL has not been included or configured.");
		console.log("Defined Shell Script: " + process.env.USE_SHELL_SCRIPT);
		console.log("\nIf you want to launch Hummingbird using this script, make sure you define the type of shell that should be used to execute it.");
		console.log("Supported shell types include: BASH, CMD");

		console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
	}
	else {

		let shellExecCommand = '';

		switch (process.env.USE_SHELL) {
			case "BASH" :
				DebugLog("\nLaunch HB via a Bash Script...\n");

				shellExecCommand = '"' + process.env.USE_SHELL_SCRIPT
								+ '" "' + process.env.HUMMINGBIRD_PLAYER_PATH
								+ '" "' + projectRoot
								+ '" "' + process.argv[2]
								+ '"';

				if (process.env.HUMMINGBIRD_ARGS && process.env.HUMMINGBIRD_ARGS.length > 0) {
					shellExecCommand += ' "' + process.env.HUMMINGBIRD_ARGS + '"';
				};

				DebugLog("\nshellExecCommand: " + shellExecCommand);

				if (runtimePlatform === 'win') {
					DebugLog('Bash Request on Windows try bash.exe...');
					HummingbirdProcess = spawn('bash.exe', ['-c', shellExecCommand]);

					HummingbirdProcess.on('error', (err) => {
						console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

						console.log("\nERROR");
						console.log("\nNo Supported BASH Executable Could be Found on Your Windows Environment.");
						console.log("Check your environment and if you have a bash.exe installed,");
						console.log("make sure that it is on your environment paths.");
						console.log("\n Bash script Hummingbird Player launch Terminated !");

						console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
						return;
					});
				}
				else {
					HummingbirdProcess = exec(shellExecCommand);

					HummingbirdProcess.on('error', (err) => {
						console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

						console.log("\nERROR");
						console.log("\nAn unknown error occured launching your bash script on your nix environment.");
						console.log("\nerror: " + err);
						console.log("\n Bash script Hummingbird Player launch Terminated !");

						console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
						return;
					});
				}
				//HummingbirdProcess = spawn('/bin/sh', ['-c', shellExecCommand]);
				break;
			case "CMD" :
				if (runtimePlatform !== 'win') {
					console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

					console.log("\nERROR");
					console.log("\nCMD scripts are only supported on Windows environments.");
					console.log("\nSwitch to a Bash script for launching the Hummingbird Player or remove your script in the ENV and let NodeJS spawn the Hummingbird Player process.");
					console.log("\nCMD script Hummingbird Player launch Terminated !");

					console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
					return;
				}
				shellExecCommand = process.env.USE_SHELL_SCRIPT
								+ ' ' + process.env.HUMMINGBIRD_PLAYER_PATH
								+ ' ' + projectRoot
								+ ' ' + process.argv[2]
								+ '';

				if (process.env.HUMMINGBIRD_ARGS && process.env.HUMMINGBIRD_ARGS.length > 0) {
					shellExecCommand += ' "' + process.env.HUMMINGBIRD_ARGS + '"';
				};

				DebugLog("\nshellExecCommand: " + shellExecCommand);
				DebugLog("\n\n");

				HummingbirdProcess = spawn('cmd.exe', ['/c', shellExecCommand]);

				HummingbirdProcess.on('error', (err) => {
					console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

					console.log("\nERROR");
					console.log("\nAn unknown error occured launching your CMD script.");
					console.log("\nerror: " + err);
					console.log("\n CMD script Hummingbird Player launch Terminated !");

					console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
					return;
				});
				break;
			default :
				console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

				console.log("\nERROR");
				console.log("\nUnhandled USE_SHELL Type :  '" + process.env.USE_SHELL + "'");
				console.log("\nPlease check the 'USE_SHELL' property in your .env file.");
				console.log("Supported Options Include: 'BASH' or 'CMD'");

				console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
				break;
		}
	}
}

if (!HummingbirdProcess) {
	let execCommand = "";
	// No Known Platform Specific Variances Needed Right Now, So this is Placeholder
	switch (runtimePlatform) {
		case "nix" :
			break;
		case "win" :
			break;
		default :
			// See the setting of runtimePlatform, this should never be used
			console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

			console.log("\nERROR");
			console.log("\nUnhandled Platform :  '" + runtimePlatform + "'");
			console.log("\nHummingbird Player launch cancelled !");

			console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
			return;
	}

	execCommand = '"' + process.env.HUMMINGBIRD_PLAYER_PATH + '" --args --root="' + projectRoot + '" --url=coui://"' + process.argv[2] + '"';

	if (process.env.HUMMINGBIRD_ARGS && process.env.HUMMINGBIRD_ARGS.length > 0) {
		execCommand += " " + process.env.HUMMINGBIRD_ARGS;
	};

	DebugLog("\nExec Command: " + execCommand);
	DebugLog("\n\n");
	HummingbirdProcess = exec(execCommand);
}


if (!HummingbirdProcess) {
	console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

	console.log("\nERROR");
	console.log("\nThe Hummingbird Player process failed to start.");
	console.log("\nPlease check your ENV configs and the command you used to launch the Player.");

	console.log("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
	return;
}

// Bind the Hummingbird Data Streams
HummingbirdProcess.stdout.on('data', (data) => {
	console.log(data.toString());
});

HummingbirdProcess.stderr.on('data', (data) => {
	console.log(data.toString());
});

HummingbirdProcess.on('exit', (code) => {
	console.log(`Child exited with code ${code}`);
});
