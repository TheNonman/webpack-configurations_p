
require('dotenv').config();

const path = require('path');
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const projectRoot = path.resolve(__dirname, '../../');

module.exports = {
	mode: 'development',
	// base directory, an absolute path, for resolving entry points and loaders from configuration
	// By default the current directory is used, but it's recommended to pass a value in your configuration.
	// This makes your configuration independent from CWD
	context: path.resolve(__dirname, '../../'),
	entry: {
		app: './AppSource/SOURCE_JS/index.js',
		print: './AppSource/SOURCE_JS/print.js'
	},
	output: {
		filename: 'js/[name].bundle.js',
		path: path.resolve(projectRoot, '_Staging'),
	},
	performance: {
		assetFilter: function(assetFilename) {
			// hints: 'warning' DEFAULT
			// maxEntrypointSize: 250000  (BYTES) DEFAULT
			// maxAssetSize : 250000  (BYTES) DEFAULT
			//
			// We are only worried about big JavaScript and CSS files
			// other assets such as images are not an issue
			return assetFilename.endsWith('.js') || assetFilename.endsWith('.css')
		},
	},
	resolve: {
		// Supported 'extension-less' paths (e.g. "import File from '../path/to/file';" for '../path/to/file.js')
		extensions: ['*', '.js', '.jsx'],
	},
	module: {
		// Modules: Module Contexts
		strictExportPresence: true,// makes missing exports an error instead of warning
		rules: [
			{
				// Read .css files referenced via imports in loaded JS and store their content in the bundled JS
				// The stored CSS content is then injected in to the DOM's Header in a Style element (tag) @ runtime
				test: /\.css$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader
					},
					'css-loader',
					'resolve-url-loader'
				]
			},
			{
				// Copies image files referenced via imports in loaded JS
				// and via CSS files referenced via imports in loaded JS
				// The images found are copied to the Output Path (relative to the base Webpack Output Path) during builds
				// The bundled JS output contains URLs to the image files adjusted by the publicPath
				test: /\.(png|svg|jpg|gif)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							// Path Images Referenced in JS source is written to
							outputPath: 'images',
							// Adjustment to Runtime loading paths
							publicPath: 'images'
						}
					}
				]
			},
			{
				// Copies font files referenced via imports in loaded JS
				// and via CSS files referenced via imports in loaded JS
				// The fonts found are copied to the Output Path (relative to the base Webpack Output Path) during builds
				// The bundled JS output contains URLs to the font files adjusted by the publicPath
				test: /\.(woff|woff2|eot|ttf|otf)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							// Path Images Referenced in JS source is written to
							outputPath: 'fonts',
							// Adjustment to Runtime loading paths
							publicPath: 'fonts'
						}
					}
				]
			},
			{
				// Reads XML files referenced via imports in loaded JS and stores their data in the bundled JS
				// NOTE: This can be done with JSON files referenced via imports in loaded JS without additional loader support.
				test: /\.xml$/,
				use: [
					'xml-loader'
				]
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin( {
									title: 'Output Management',
									filename: 'printpage.html',
									template: 'AppSource/SOURCE_HTML/printpage_template.html'
		}),
		new ManifestPlugin( {
								filename: 'WebpackManifest.json'
		}),
		new MiniCssExtractPlugin({
			filename: './css/[name].css'
		})
	]
};
