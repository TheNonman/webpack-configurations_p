
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const merge = require('webpack-merge');

const projectRoot = path.resolve(__dirname, '../');
const wpCommonConfig = require('./webpack.common.config.js');

module.exports = merge(wpCommonConfig, {
	mode: 'production',
	plugins: [
		new CleanWebpackPlugin(['_Staging'], { root: path.resolve(__dirname , '../../'), allowExternal: false }),
		new CopyWebpackPlugin([
			{ from: 'AppSource/SOURCE_HTML/index.html', to: './index.html' }
		])
	],
	devtool: 'source-map'
})
