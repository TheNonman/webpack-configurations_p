
const CopyWebpackPlugin = require('copy-webpack-plugin');
const merge = require('webpack-merge');

const wpCommonConfig = require('./webpack.common.config.js');

module.exports = merge(wpCommonConfig, {
	output: {
		publicPath: '/'
	},
	plugins: [
		new CopyWebpackPlugin([
			{ from: 'AppSource/SOURCE_HTML/index.html', to: './index.html' }
		])
	]
})
