module.exports =
{
	"extends": [
		"../stylelint.config.scss.js"
	],
	"rules": {
		//
		//  Validation Rules
		// ====================================================================
		// ====================================================================

		"font-family-no-missing-generic-family-keyword": [ null ], // Disabled on Compiled CSS Pass for generated Icon Font
		"no-duplicate-selectors": [ null ], // Disabled on Compiled CSS Pass for Starting Resets

		"scss/at-rule-no-unknown": null,
		"at-rule-no-unknown": [ true,
			// Validates :  At-rules defined in the CSS Specifications (i.e. @media, @charset, etc.)
						{	"severity": "error",
							"ignoreAtRules": [] // Add Valid, but Non-Standard At-rules to this Array
			}
		],
		"no-invalid-double-slash-comments": [ true,
						{	"severity": "error",
							"message": "Single-line (AKA Double-Slash) Comments are Not Valid in CSS !!!" }
		]
	}
}
