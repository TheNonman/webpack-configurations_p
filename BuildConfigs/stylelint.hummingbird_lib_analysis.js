module.exports =
{
	"extends": [
	],
	"rules": {
		//
		//  Validation Rules
		// ====================================================================
		// ====================================================================

		"at-rule-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed at-rules.
			// NOTE: This rule ignores @import in Less.
			// Example :  ["keyframes"] would disallow any @keyframes definitions, including vendor
			// prefixed @-moz-keyframes
						{	"severity": "error" }
		],
		"comment-word-blacklist": [ [],
			// Validates :  Against a specified blacklist of words that cannot be included in comment blocks.
			// Example :  ["TODO"] would disallow any comment like '/* TODO: something */'
						{	"severity": "error" }
		],
		"declaration-property-unit-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed property and unit pairs
			// Example :  ["font-size": ["em", "px"]] would disallow use of 'em' and 'px' units for font-size properties,
						{	"severity": "error" }
		],
		"declaration-property-value-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed property and value pairs
			// Example :  ["position": ["fixed"]] would disallow use of 'fixed' as a value for the position property,
						{	"severity": "error" }
		],
		"function-blacklist": [ ["calc"],
			// Validates :  Against a specified blacklist of disallowed functions (i.e. calc, scale, rgba, etc.).
						{	"severity": "error",
							"message": "Hummingbird DOM Renderer does Not Support the CSS Calc function !!!" }
		],
		"function-url-scheme-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed URL schemes (i.e. ["ftp", "/^http/"], etc.).
						{	"severity": "error" }
		],
		"media-feature-name-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed media feature names
			// Example :  ["max-width"] would disallow a media selector like, '@media (max-width: 50em) {}'
			// NOTE: Caveat: Media feature names within a range context are currently ignored.
						{	"severity": "error" }
		],
		"media-feature-name-value-whitelist": [ {},
			// Validates :  Against a specified whitelist of allowed media feature name and value pairs.
			// If a media feature name is found in this object, only its whitelisted values are allowed.
			// If a media feature name is not included in this object, anything goes for its value.
			// Example :  { "min-width": ["768px", "1024px"] } would disallow any @media (min-width) that
			// had a value other than 768px or 1024px.
						{	"severity": "error" }
		],
		"property-blacklist": [ ["background", "border", "font", "margin", "padding"],
			// Validates :  Against a specified blacklist of disallowed CSS properties (i.e. "animation", "text-rendering", etc.).
						{	"severity": "error",
							"message": "Hummingbird DOM Renderer does Not Support CSS Shorthand Properties !!!"
			}
		],
		"selector-attribute-operator-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed attribute operators
			// Example :  ["*="] would disallow [class*="test"],
						{	"severity": "error" }
		],
		"selector-combinator-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed selector combinators
			// Example :  [">", " "] would disallow selectors like 'a > b {}' & 'a b {}',
						{	"severity": "error" }
		],
		"selector-pseudo-class-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed pseudo-class selectors
			// Example :  ["hover", "/^nth-/"] would disallow selectors like 'a:hover' & 'a:nth-child'
			// NOTE: This rule ignores selectors that use variable interpolation e.g. :#{$variable} {}
						{	"severity": "error" }
		],
		"selector-pseudo-element-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed pseudo-element selectors
			// Example :  ["before", "after"] would disallow selectors like 'a::before' & '.myMenu::after',
						{	"severity": "error" }
		],
		"unit-blacklist": [ [""],
			// Validates :  Against a specified blacklist of disallowed value units (i.e. "px", "em", "deg", "%", etc.).
						{	"severity": "error" }
		],
	}
}
