# Webpack-Configurations_p

Implementations/samples of various configuration options for Webpack.

## Build & Serve Options

### **Straight Webpack**

These options and flows use Webpack only. No other tasking, building, or serving libraries are used.

`npm run build` - Development build: inline source maps and no minification.

`npm run build_prod` - Production build: seperate source map files and code minification.

`npm run dev_serve` - Development build (inline source maps and no minification) and serve project content (WebPack Dev Server). Constent is served via localhost:8080. 

*Note:* The Webpack Dev Server builds and serves the content from a memory cache space. That is, whether or not a build has been executed (generating a "_Staging" directory of content), the content that is actually served is **not** viewable in the project directory.

### **Straight Serve**

`npm run serve` - Launch an Express server, that will serve _whatever_ is currently in the _Staging directory. This requires one of the build processes to have been executed so that there is a _Staging directory with built content in it. This can be combinend with (serverless) build processes that include watches, by executing this command in a separate process thread.

### **Webpack Express Server**

This option builds and serves the project content. It is similar to the Webpack Dev Server option above, but the server used is Express. Instead of using the Webpack Dev Server, an Express server is started, using the WebPack-Dev-Middleware to compile the project content (and recompile it as changes to the source files are made).

As with the note above on the Webpack Dev Server, this flow builds and serves the content from a memory cache space, not a directory accessible in the project directory.

`npm run dev_express` - Launch an Express server with the WebPack-Dev-Middleware for compiling the Webpack content. Constent is served via localhost:3300

### **Webpack via Grunt**

These options and flows are based on Grunt tasking, including having Grunt execute Webpack compiling as necessary. Other processes that are not part of the bundle compilation are handled by Grunt tasks that have no conection to Webpack.

`grunt BasicBuild` - Basic Grunt development build of the project content: inline source maps and no minification.

`grunt ProdBuild` - Basic Grunt production build of the project content: seperate source map files and code minification.

`grunt Work` - Basic Grunt development, build per the BasicBuild above, but with an Express server started (serving the _Staging directory), watches activated on both the Webpack build process and the Grunt build processed content, and BrowserSync for activating browser reloads when the served content changes.

### **Launch in Hummingbird Player**

`grunt run:launch_hb` - Launches built project content in the "_Staging" directory in Hummingbird Player (a build must exist before running).


# Implementation Notes

## Webpack without Grunt

When a Webpack process is executed outside of a Grunt process context, it loads the appropriate configuration from a Webpack config file located in 'BuildConfigs/WebPack'. This directory contains different config files for different Webpack process intents. Each of the various config options begin by loading the content of webpack.common.config.js, which wraps a starting Webpack configuration. The default settings in the webpack.common.config.js provide a base to start from, with settings that are (at least for the most part) common to all use cases. The other ("concrete") configs then change or add to those defaults as necessary.

## Webpack from Grunt

When a Webpack process is executed by a Grunt process, the appropriate configuration is provided by the Webpack Grunt task config ('BuildConfigs/Grunt/TaskConfigs/webpack.js'). It is important to note that (like the 'Grunt-free' implementation) the task config begins by loading the content of webpack.common.config.js (from 'BuildConfigs/WebPack'). Unlike the 'Grunt-free' implementation though, changes/additions to the defaults provided by the common config are defined directly in the task config, rather than from other files. Thus, the only file in the 'BuildConfigs/WebPack' directory that is used by the Grunt flows is webpack.common.config.js.

# TODO  LIST


