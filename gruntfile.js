'use strict';
//
//
// # NOTES
// ================================================================================================
// ================================================================================================



//
// # GRUNT / NODE	FILE	MATCHING	(GLOBBING)
// ----------------------------------------------
//
// Restrict matching to the content of one directory level down (i.e. single directory nesting):
//		'SomeRootDirectory/{,*/}*.js'
// Full Recursion, matching the content files in all subdirectoy levels:
//		'SomeRootDirectory/**/*.js'
// Full Recursion, matching All content:
//		'SomeRootDirectory/**'
// Skip a directory during recursion:
//		'!**/SkipThisDirectoryName/**'


//
//
// # Initialization of Any External Processor Objs
// ================================================================================================
// ================================================================================================

var LogFile = require("logfile-grunt");
//var BackstopJS = require('backstopjs');
//var BackstopConfigPath = "./BuildConfigs/backstop.json";


//
//
// # Definition of Grunt Task Handling
// ================================================================================================
// ================================================================================================

module.exports = function(grunt)
{
	//
	// # Load Sub-modules (i.e. Grunt Tasks)
	// (Simplify with "matchdep" or "load-grunt-tasks" or get fancy with "load-grunt-config")
	// --------------------------------------------------------------------------------------------

	var Chalk = require("chalk");
	var CommandExists = require('command-exists').sync;
	var exec = require('child_process').exec;

	// Load All Grunt Task Types (i.e. All Node Modules that Start with "grunt-")
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	// Load All Available Custom Grunt Tasks
	grunt.task.loadTasks('./BuildConfigs/Grunt/CustomTasks');


	//
	// # Configure App Structure References
	// --------------------------------------------------------------------------------------------
	var NodePkg = grunt.file.readJSON('package.json');

	var BuildConfigs = {
		src: {
			root: "AppSource",
			css: "AppSource/SOURCE_STYLES",
			css_3P: "AppSource/SOURCE_STYLES/a_third_party",
			fonts: "AppSource/SOURCE_FONTS",
			fonts_to_build: "AppSource/SOURCE_SVG4FONTS",
			html: "AppSource/SOURCE_HTML",
			html_ejs: "AppSource/SOURCE_HTML-EJS",
			images: "AppSource/SOURCE_IMAGES",
			js: "AppSource/SOURCE_JS",
			js_3P: "AppSource/SOURCE_JS/a_third_party",
			js_3P_Bower: "AppSource/bower_components",
			templates: "AppSource/SOURCE_HTML/templates"
		},
		temp: {
			root: "__Temp",
			css: "__Temp/css",
			css_3P: "__Temp/css/a_third_party",
			css_pre_source: "__Temp/css_compass_source",
			fonts: "__Temp/fonts",
			fonts_built: "__Temp/fonts_built",
			images: "__Temp/images",
			js: "__Temp/js",
			js_3P: "__Temp/js/a_third_party",
			templates: "__Temp/templates"
		},
		staging: {
			root: "_Staging",
			css: "_Staging/css",
			css_3P: "_Staging/css/a_third_party",
			fonts: "_Staging/fonts",
			images: "_Staging/img",
			js: "_Staging/js",
			js_3P: "_Staging/js/a_third_party",
			templates: "_Staging/templates"
		},
		logs: {
			base: "__BuildLogs"
		},
		finalFileNames: {
			css_Print: 'print.css',
			css_Screen: 'screen.css',
			js_Global: 'global.js'
		},
		staging_URLs: {
			main: "http://localhost:3000/"
		}
	};

	var JSHintOptions = {
		Production: {
			debug: false,
			node: true,
			browser: true,
			jquery: true, // Support for jQuery $ Def
			esnext: true,
			bitwise: true,
			curly: false,
			eqeqeq: false, // Suppress Warnings About Using == instead of ===
			immed: true,
			latedef: true,
			newcap: false,
			noarg: true,
			regexp: true,
			undef: true,
			unused: 'vars',  // Warn About Unused Variables but not Function Params (like e in an Event Handler)
			strict: true,
			trailing: false, // Allow Trailing Whitespace
			smarttabs: true,
			laxbreak: true,
			laxcomma: true, // Allow Comma-first Object Definition Syntax
			white: false // Don't Warn About Whitepace/tab Counts
		},
		Debug : {
			debug: true,
			node: true,
			browser: true,
			jquery: true, // Support for jQuery $ Def
			esnext: true,
			bitwise: true,
			curly: false,
			eqeqeq: false,  // Suppress Warnings About Using == instead of ===
			immed: true,
			latedef: true,
			newcap: false,
			noarg: true,
			regexp: true,
			undef: true,
			unused: 'vars',  // Warn About Unused Variables but not Function Params (like e in an Event Handler)
			strict: true,
			trailing: false, // Allow Trailing Whitespace
			smarttabs: true,
			laxbreak: true,
			laxcomma: true, // Allow Comma-first Object Definition Syntax
			white: false // Don't Warn About Whitepace/tab Counts
		}
	};


	var LogFileOptions = {
		filePath: BuildConfigs.logs.base + "/Grunt.log",
		clearLogFile: true,
		keepColors: false,
		textEncoding: "utf-8"
	};

	var GruntBuildOptions = {
								config: { src: "./BuildConfigs/Grunt/TaskConfigs/*.js" },
								NodeProjectPkg: NodePkg,
								BuildConfigs: BuildConfigs,
								JSHintOptions: JSHintOptions
							};

	var TaskConfigs = require('load-grunt-configs')(grunt, GruntBuildOptions);


	//
	// # Helper Method Definitions
	// --------------------------------------------------------------------------------------------

	var RunCLI_Cmd = function(item, callback) {
		process.stdout.write('Running CLI Command, "' + item + '"...\n');
		var cmd = exec(item);
		cmd.stdout.on('data', function (data) {
			grunt.log.writeln(data);
		});
		cmd.stderr.on('data', function (data) {
			grunt.log.errorlns(data);
		});
		cmd.on('exit', function (code) {
			if (code !== 0) throw new Error('Running CLI Command, "' + item + '" failed');
			grunt.log.writeln('done\n');
			if (callback) {
				callback();
			}
		});
	};


	//
	// # Configure Grunt Tasks
	// --------------------------------------------------------------------------------------------

	grunt.initConfig(TaskConfigs);


	//
	// # Register (Executable) Grunt Task Aliases
	// --------------------------------------------------------------------------------------------

	// Define the DEFAULT task - executed when running grunt with no arguments
	grunt.registerTask('default', ['Help']);


	grunt.registerTask('BasicBuild', 'Basic One-time build (No Watches Set).',
		function() {
			grunt.task.run([
				'StartProcessLog',
//				'eslint',
//				'stylelint:scss',
				'clean:full',

//				'copy:pre_css_source',
//				'webfont', // ENABLE - Built Fonts

//				'compass:test',
//				'stylelint:temp_css',
				//'sync:js_ToTemp',
				//'sync:js3P_ToTemp',
//				'babel',
//				'copy:bower_components_debug',
//				'sync:css_ToTemp',
//				'sync:css_ToStaging',
				//'sync:js_ToStaging',
//				'rollup:product_react',
//				'rollup:product_preact',
//				'rollup:reactdocs',
				'sync:html_ToStaging',
				'sass',
				'copy:image_files_ToTemp',
				'copy:image_files_ToStaging',
				'sync:fonts_ToStaging',
				'webpack:dev'

//				'sync:fonts_built_ToStaging', // ENABLE - Built Fonts
//				'ejs'
			]);
		}
	);

	grunt.registerTask('ProdBuild', 'One-time Production build (No Watches Set).',
		function() {
			grunt.task.run([
				'StartProcessLog',
				'clean:full',

				'sync:html_ToStaging',
				'copy:image_files_ToTemp',
				'copy:image_files_ToStaging',
				'sync:fonts_ToStaging',
				'webpack:prod'

			]);
		}
	);


	grunt.registerTask('Docs', 'Generate (Update) the API documentation with JSDoc.',
		function() {
			grunt.task.run([
				'StartProcessLog',
				'jsdoc'
			]);
		}
	);


	grunt.registerTask('Help', 'Print project info and available grunt tasks.  < DEFAULT >',
		function() {
			grunt.task.run([
				'StartProcessLog',
				'availabletasks'
			]);
		}
	);


	grunt.registerTask('Nuke', 'Clears All Build Content and Strips Down to Only Source (NPM Install will be Required to Work Again).',
		function() {
			grunt.task.run([
				'clean:strip_to_source'
			]);
		}
	);


	grunt.registerTask('Tests', 'Run Defined Tests.',
		function (  ) {
			// NOTE: the BackstopJS tests are executed targeting URLs on the Express server (which must start first).
			// This is required to be able to fully-resolve both absolute and relative paths in the server context.
			grunt.task.run([
				'express:dev',
				'Test_Backstop:test',
				'express:dev:stop'
			]);

			grunt.log.writeln("Tests COMPLETE");
		}
	);


	grunt.registerTask('TestsApproved', 'Confirm that Changes in Testing are Correct and Update Regression Test Assets.',
		function (  ) {
			grunt.task.run([
				'Test_Backstop:approve'
			]);
		}
	);


	grunt.registerTask('Test_Backstop', 'BackstopJS Visual Regression Testing Integration',
		function ( backstopCommand ) {
			// backstopCommand is either 'reference', 'test', or 'openReport'
			var done = this.async();
			BackstopJS(backstopCommand, { config: BackstopConfigPath }).then(function() {
				done(true);
			}).catch(function() {
				done(true);
			});
		}
	);


	grunt.registerTask('MultiWork', 'Starts the Development Build Cycle, Express Web Server, and Watches. Opens and updates in Chrome AND All Other Registered Browsers.',
		function() {
			grunt.task.run([
				'BasicBuild',
				'express:dev',
				'browserSync_ByPlatform:all',
				'watch'
			]);
		}
	);


	grunt.registerTask('Work', 'Starts the Development Build Cycle, Express Web Server, and Watches. Opens and updates in Chrome only.',
		function() {
			grunt.task.run([
				'BasicBuild',
				'express:dev',
				//'open', // Open task removed - This is handled by BrowserSync
				'browserSync_ByPlatform:one',
				'concurrent:webpack_served'
			]);
		}
	);


	// Default task(s).
	grunt.registerTask('StartProcessLog', 'Adds Process Info in the Log and Console',
		function() {

			new LogFile(grunt, LogFileOptions);

			grunt.log.writeln();
			grunt.log.writeln();
			grunt.log.writeln();
			grunt.log.writeln(Chalk.green('Starting Grunt Tasking for...'));
			grunt.log.writeln(Chalk.green('	->	NPM Project Name: ' + grunt.config('NodeProjectPkg.name')));
			grunt.log.writeln(Chalk.green('	->	NPM Project Version: ' + grunt.config('NodeProjectPkg.version')));
			grunt.log.writeln(Chalk.green('	->	Using Grunt Version: ' + Chalk.blue(grunt.version)));

			var startTime = new Date();
			grunt.log.writeln(Chalk.green('	->	Process Started: ' + Chalk.blue(startTime)));
			grunt.log.writeln(Chalk.green('================================================================================'));
			grunt.log.writeln(Chalk.green('================================================================================'));
			grunt.log.writeln("");
		}
	);



	grunt.registerTask('Build_OpenFl', 'Builds a Single Test OpenFL Project',
		function() {
			if (BuildConfigs.openFL_project) {
				if ( grunt.file.exists( BuildConfigs.openFL_project) ){
					grunt.log.writeln(Chalk.green('OpenFL Project File Exists: ' + BuildConfigs.openFL_project));
					var commandName = "openfl";
					if (CommandExists(commandName)) {
						grunt.log.writeln(Chalk.green(commandName + '  IS  IN  FACT  INSTALLED !'));

						var async = require('async');
						var done = this.async();

						async.series({
							openfl: function(callback){
								RunCLI_Cmd(commandName + ' build ' + BuildConfigs.openFL_project + ' html5 -v', callback);
							}
						},
						function(err, results) {
							if (err) done(false);
							done();
						});
					}
					else {
						grunt.log.writeln(Chalk.red("ERROR: The CLI Command '" + commandName + "' is Not Available. Verify you have installed " + commandName));
					}
				}
				else {
					grunt.log.writeln(Chalk.green("No OpenFL Project File ('" + BuildConfigs.openFL_project + "') Present. Skipping openfl build"));
				}
			}
			else {
				grunt.log.writeln(Chalk.green('No configuration for OpenFL Content'));
			}
		}
	);
};
